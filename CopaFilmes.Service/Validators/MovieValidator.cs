﻿using CopaFilmes.Domain.Entities;
using FluentValidation;
using System;

namespace CopaFilmes.Service.Validators
{
    public class MovieValidator : AbstractValidator<Movie>
    {
        public MovieValidator()
        {
            RuleFor(c => c)
                    .NotNull()
                    .OnAnyFailure(x =>
                    {
                        throw new ArgumentNullException("Can't found the object.");
                    });

            RuleFor(c => c.Titulo)
                .NotEmpty().WithMessage("Is necessary to inform the Title.")
                .NotNull().WithMessage("Is necessary to inform the Title.");

            RuleFor(c => c.Ano)
                .NotEmpty().WithMessage("Is necessary to inform the Year")
                .NotNull().WithMessage("Is necessary to inform the Year");

            RuleFor(c => c.Nota)
                .NotEmpty().WithMessage("Is necessary to inform the Rating.")
                .NotNull().WithMessage("Is necessary to inform the Rating");
        }
    }
}
