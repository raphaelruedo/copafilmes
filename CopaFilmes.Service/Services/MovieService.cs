﻿using CopaFilmes.Domain.Entities;
using CopaFilmes.Infra.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CopaFilmes.Service.Services
{
    public class MovieService
    {
        private MovieRepository repository = new MovieRepository();

        public IEnumerable<Movie> GetAll()
        {
            return repository.GetAll();
        }

        public Match GetWinner(List<Movie> model)
        {
            IQueryable<Movie> movies = model.AsQueryable().OrderBy(m => m.Titulo);
            IQueryable<Movie> descMovies = movies.OrderByDescending(m => m.Titulo);

            List<Match> matchs = new List<Match>();

            foreach (Movie movie in movies)
            {
                Match match = new Match
                {
                    TimeA = movie,
                    TimeB = descMovies.ElementAt(movies.ToList().FindIndex(m => m.Id == movie.Id)),
                };

                match.Winner = Convert.ToDouble(match.TimeA.Nota) > Convert.ToDouble(match.TimeB.Nota) ? match.TimeA : match.TimeB;

                if (movies.ToList().FindIndex(m => m.Id == movie.Id) == 4)
                {
                    break;
                }
                else
                {
                    matchs.Add(match);
                }

            }


            List<Movie> semiVencedores = matchs.Select(o => o.Winner).ToList();
            List<Movie> Vencedores = new List<Movie>();
            List<Movie> Vencedores2 = new List<Movie>();

            List<Match> partidasfinal = new List<Match>();

            foreach (var final in semiVencedores)
            {
                if (semiVencedores.FindIndex(c => c.Id == final.Id) % 2 == 0)
                {
                    Vencedores.Add(final);
                }
                else
                {
                    Vencedores2.Add(final);
                }
            }

            foreach (var venc in Vencedores)
            {
                Match partida = new Match
                {
                    TimeA = venc,
                    TimeB = Vencedores2.ElementAt(Vencedores.ToList().FindIndex(m => m.Id == venc.Id)),
                };

                partida.Winner = Convert.ToDouble(partida.TimeA.Nota) > Convert.ToDouble(partida.TimeB.Nota) ? partida.TimeA : partida.TimeB;

                partidasfinal.Add(partida);
            }


            var finalistas = partidasfinal.Select(p => p.Winner).ToList();

            Match partidaFinal = new Match()
            {
                TimeA = finalistas.FirstOrDefault(),
                TimeB = finalistas.Last()
            };

            partidaFinal.Winner = Convert.ToDouble(partidaFinal.TimeA.Nota) > Convert.ToDouble(partidaFinal.TimeB.Nota) ? partidaFinal.TimeA : partidaFinal.TimeB;

            partidaFinal.Second = partidaFinal.Winner.Id == partidaFinal.TimeA.Id ? partidaFinal.TimeB : partidaFinal.TimeA;

            return partidaFinal;
        }
    }
}
