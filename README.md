# Copa Filmes

Projeto para processo seletivo utilizando asp net core e react js

## Getting Started

1-Clonar este repositório
2-Abrir a soluação no visual studio ou em algum editor de texto(caso seja no editor de texto pular para passo 6)
3-No Visual studio abrir o arquivo "CopaFilmes.SLN"
4-Dar um Clean na Solution e depois um Rebuild para que possa ser atualizados todos pacotes.
5-Rodar aplicação normalmente.
6-Abrir a pasta do projeto no editor de texto
7-No Terminal rodar "dotnet clean"
8-Depois "dotnet build"
9-Rodar aplicação com "dotnet run" normalmente.

obs: caso achar necessário rodar um npm install na pasta "ClientApp".

## Authors

* **Raphael Ruedo** - *Initial work* - [RaphaelRuedo](https://github.com/raphaelruedo)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

