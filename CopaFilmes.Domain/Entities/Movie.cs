﻿namespace CopaFilmes.Domain.Entities
{
    public class Movie : BaseEntity
    {
        public string Id { get; set; }
        public string Titulo { get; set; }
        public string Ano { get; set; }
        public string Nota { get; set; }
    }
}
