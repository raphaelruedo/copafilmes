﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CopaFilmes.Domain.Entities
{
   public class Match
    {
        public Movie TimeA { get; set; }
        public Movie TimeB { get; set; }
        public Movie Winner { get; set; }
        public Movie Second { get; set; }
    }
}
