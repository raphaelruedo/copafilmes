﻿using CopaFilmes.Domain.Entities;

namespace CopaFilmes.Domain.Interfaces
{
    public interface IMovieRepository: IRepository<Movie>
    {

    }
}
