﻿using CopaFilmes.Domain.Entities;
using System.Collections.Generic;
using System.Linq;

namespace CopaFilmes.Domain.Interfaces
{
    public interface IRepository<T> where T : BaseEntity
    {
        void Insert(T obj);
        void Update(T obj);
        void Delete(int id);
        T Get(int id);
        IList<T> GetAll();
    }
}
