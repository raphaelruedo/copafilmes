﻿using CopaFilmes.Domain.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace CopaFilmes.Infra.Data.Repository
{
    public class MovieRepository
    {
        string Baseurl = "http://copafilmes.azurewebsites.net/api/";


        public IEnumerable<Movie> GetAll()
        {
            List<Movie> list = new List<Movie>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var result = client.GetAsync("filmes")
                    .ContinueWith((taskwithresponse) =>
                    {
                        var response = taskwithresponse.Result;
                        var jsonString = response.Content.ReadAsStringAsync();
                        jsonString.Wait();
                        list = JsonConvert.DeserializeObject<List<Movie>>(jsonString.Result);

                    });
                result.Wait();
            }

            return list;
        }
    }
}
