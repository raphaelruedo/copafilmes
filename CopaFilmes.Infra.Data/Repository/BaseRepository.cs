﻿using CopaFilmes.Domain.Entities;
using CopaFilmes.Domain.Interfaces;
using System.Collections.Generic;
using CopaFilmes.Infra.Data.Context;
using System.Linq;

namespace CopaFilmes.Infra.Data.Repository
{
    public class BaseRepository<T> : IRepository<T> where T : BaseEntity
    {
        private CopaFilmesContext context = new CopaFilmesContext();

        public void Insert(T obj)
        {
            context.Set<T>().Add(obj);
            context.SaveChanges();
        }

        public void Update(T obj)
        {
            context.Entry(obj).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            context.Set<T>().Remove(Get(id));
            context.SaveChanges();
        }

        public IList<T> GetAll()
        {
            return context.Set<T>().ToList();
        }

        public T Get(int id)
        {
            return context.Set<T>().Find(id);
        }
    }
}
