﻿using AutoMapper;
using CopaFilmes.Domain.Entities;
using CopaFilmes.UI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CopaFilmes.UI.AutoMapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<MovieViewModel, Movie>();
            CreateMap<MatchViewModel, Match>();
        }
    }
}
