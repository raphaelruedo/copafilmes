﻿namespace CopaFilmes.UI.ViewModel
{
    public class MovieViewModel
    {
        public string id { get; set; }
        public string Titulo { get; set; }
        public string Ano { get; set; }
        public string Nota { get; set; }
    }

    public class MatchViewModel
    {
        public MovieViewModel TimeA { get; set; }
        public MovieViewModel TimeB { get; set; }
        public MovieViewModel Winner { get; set; }
        public MovieViewModel Second { get; set; }
    }
}
