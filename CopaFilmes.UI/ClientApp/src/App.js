import React, { Component } from 'react';

import './css/App.css';
import 'font-awesome/css/font-awesome.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';

import { Route } from 'react-router';

import Header from './components/header/header';
import Apppage from './components/apppage/apppage';
import TecnologyPage from './components/tecnology/tecnology';
import FooterPage from './components/footer/footer';
import MovieList from './components/movie/movieList';
import MovieCard from './components/movie/movieCard';

class App extends Component {
  displayName = App.name

  render() {
    return (
      <div className="App"> 
      <Header />
        <Route path='/tecnology' component={TecnologyPage} />
        <Route path='/movies' component={MovieList} />
         <Route exact path='/' component={Apppage} />
         <Route path='/result' component={MovieCard} />
        <FooterPage />
      </div>
    );
  }
}

export default App;

