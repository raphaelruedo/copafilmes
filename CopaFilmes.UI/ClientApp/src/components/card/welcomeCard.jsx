import React from 'react';
import { Card, CardBody, Col } from 'mdbreact';


class WelcomeCard extends React.Component {
    render() {
        return (
            <Col md="12">
                <Card>
                    <CardBody>
                        <h4>Fase de Seleção</h4>
                        <hr className="divider"/>
                        <p>Selecione 8 filmes que você deseja que entre na competição e depois pressione o <br></br>botão <strong>Gerar Meu Campeonato</strong> para prosseguir.</p>
                </CardBody>
                </Card>
            </Col>
        )
    }
}

export default WelcomeCard;