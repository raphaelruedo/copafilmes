import React, { Component } from 'react';
import { Container, Row, Col, Fa } from 'mdbreact';

class TecnologyPage extends Component {
    render() {
        return (
            <div>
                <hr className="section-divider"/>
            <Container id="tecnology">
                <section className="text-center my-5">
                    <h2 className="h1-responsive font-weight-bold my-5">Tecnologias usadas</h2>
                    <Row>
                        <Col md="6">
                            <Fa icon="television" size="3x" className="cyan-text" />
                            <h5 className="font-weight-bold my-4">ReactJS</h5>
                            <p className="grey-text mb-md-0 mb-5">
                                React é uma biblioteca para criar interfaces
                            </p>
                        </Col>
                        <Col md="6">
                            <Fa icon="cubes" size="3x" className="cyan-text" />
                            <h5 className="font-weight-bold my-4">MDBReact</h5>
                            <p className="grey-text mb-md-0 mb-5">
                                Framework de interfaces baseado no Bootstrap 4 e Material Design
                            </p>
                        </Col>
                        
                    </Row>
                </section>
            </Container>
            </div>
        );
    };
}

export default TecnologyPage;