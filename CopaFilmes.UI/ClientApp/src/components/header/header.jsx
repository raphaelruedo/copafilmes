import React from 'react';
import { Navbar, NavbarBrand, NavbarNav, NavItem, NavbarToggler, Collapse, Container, Fa, NavLink  } from 'mdbreact';

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            collapse: false
        }
        this.onClick = this.onClick.bind(this);
        this.handleNavbarClick = this.handleNavbarClick.bind(this);
    }

    onClick() {
        this.setState({
            collapse: !this.state.collapse,
        });
    }

    handleNavbarClick() {
        this.setState({
            collapse: false
        });
    }
    render() {
        // const overlay = <div id="sidenav-overlay" style={{ backgroundColor: 'transparent' }} onClick={this.handleNavbarClick} />
        return (
            <div>
                 <Navbar  dark expand="md">
                 <Container>
                        <NavbarBrand>
                            <strong className="white-text">copafilmes</strong>
                        </NavbarBrand>
                        <NavbarToggler onClick={this.onClick} />
                        <Collapse isOpen={this.state.collapse} navbar>
                            <NavbarNav left>
                                <NavItem>
                                    <NavLink  to="/" className="nav-link">Início</NavLink >
                                </NavItem>
                                <NavItem>
                                    <NavLink  to="movies" className="nav-link">Campeonato</NavLink >
                                </NavItem>
                                <NavItem>
                                    <NavLink  to="tecnology" className="nav-link">Tecnologias</NavLink >
                                </NavItem>
                            </NavbarNav>
                            <NavbarNav right >
                                <NavItem>
                                    <a className="nav-link" href="https://github.com/raphaelruedo/">
                                        <Fa icon="github"></Fa>
                                    </a>
                                </NavItem>
                                <NavItem>
                                    <a className="nav-link" href="https://www.linkedin.com/in/raphael-ruedo-141406140/">
                                        <Fa icon="linkedin"></Fa>
                                    </a>
                                </NavItem>
                                <NavItem>
                                    <a className="nav-link" href="https://raphaelruedo.github.io/site/">
                                        <Fa icon="code"></Fa>
                                    </a>
                                </NavItem>
                            </NavbarNav>
                        </Collapse>
                    </Container>
                </Navbar>
                {this.state.collapse}
            </div>
        );
    }
};

export default Header;