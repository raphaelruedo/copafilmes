import React, { Component } from 'react';

import { Card, CardBody, Col, Row, Container } from 'mdbreact';

class MovieCard extends Component {
    displayName = MovieCard.name

    constructor(props) {
        let movies = JSON.parse(localStorage.getItem('vencedor'));
        super(props);
        this.state = {
            moviesData: movies
        };
    }

    render() {
        return (
            <div>
                <hr className="section-divider" />
                <form>
                    <Container id="movies">
                        <section className="text-center my-5">
                            <h2 className="h1-responsive font-weight-bold my-5">Campeonato de Filmes</h2>
                            <Row>
                                <Col md="12">
                                    <Card>
                                        <CardBody>
                                            <h4>Resultado Final</h4>
                                            <hr className="divider" />
                                            <p>Veja o resultado final do Campeonato de filmes de forma simples e r�pida.</p>
                                        </CardBody>
                                    </Card>
                                </Col>
                            </Row>
                            <Row>
                                <Col md="12">
                                    <Card>
                                        <CardBody>
                                            <Row>
                                                <Col md="2">
                                                    <h1>1�</h1>
                                                </Col>
                                                <Col md="8">
                                                    {this.state.moviesData.winner.titulo}
                                                </Col>
                                                <Col md="2">
                                                    {this.state.moviesData.winner.nota}
                                                </Col>
                                            </Row>
                                        </CardBody>
                                    </Card>
                                </Col>
                            </Row>
                            <Row>
                                <Col md="12">
                                    <Card>
                                        <CardBody>
                                            <Row>
                                                <Col md="2">
                                                    <h1>2�</h1>
                                                </Col>
                                                <Col md="8">
                                                    {this.state.moviesData.second.titulo}
                                                </Col>
                                                <Col md="2">
                                                    {this.state.moviesData.second.nota}
                                                </Col>
                                            </Row>
                                        </CardBody>
                                    </Card>
                                </Col>
                            </Row>
                        </section>
                    </Container>
                </form>
            </div>
        );

    }
}

export default MovieCard;