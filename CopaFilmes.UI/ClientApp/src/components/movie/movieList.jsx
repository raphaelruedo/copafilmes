import React, { Component } from 'react';
import { Redirect } from 'react-router'

import axios from 'axios';

import { Container, Row, Col, Fa, Button, Card, CardBody, Input, Stepper, Step } from 'mdbreact';
import WelcomeCard from '../card/welcomeCard';


var movieList = [];
var countMovies = 0;

class MovieList extends Component {

    constructor(props) {
        super(props);
        this.state = { movies: [], loading: true, selectedMovies: [], redirect: false, countMovies: 0 };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleMovie = this.handleMovie.bind(this);

        movieList = [];
        fetch('api/Movie/GetMovies')
            .then(response => response.json())
            .then(data => {
                this.setState({ movies: data, loading: false, countMovies: 0 });
            });
    }

    handleMovie(movie) {

        if (document.getElementById(movie.id).checked) {
            movieList.push(movie);
        } else {
            movieList.splice(movieList.findIndex(v => v.id === movie.id), 1);
        }
        this.setState({countMovies:movieList.length});

    }


    handleSubmit(event) {
        event.preventDefault();

        if (movieList.length !== 8) {
            alert('Campeonato só pode ser gerado com 8 filmes.');
        } else {
            var movies = JSON.stringify(movieList);
            axios({
                url: 'api/Movie/Championship',
                headers: {
                    'Content-Type': 'application/json'
                },
                method: 'post',
                data: movies
            })
                .then(function (response) {
                    localStorage.setItem('vencedor', JSON.stringify(response.data));
                    console.log(response.data);
                })
                .then(() => this.setState({ redirect: true, data: "pass" }))
                .catch(function (error) {
                    console.log(error);
                });
        }


    }


    render() {
        const { redirect } = this.state;

        if (redirect) {
            return <Redirect to='/result/' />;
        }

        return (
            <div>
                <hr className="section-divider" />
                <form>
                    <Container id="movies">
                        <section className="text-center my-5">
                            <h2 className="h1-responsive font-weight-bold my-5">Campeonato de Filmes</h2>
                            <Row>
                                <WelcomeCard />
                            </Row>

                            <Row>
                                <Col md="6" className="d-flex justify-content-start">
                                    <div className="p-2 text-justify">Selecionados <br />{this.state.countMovies} de 8 Filmes</div>
                                </Col>
                                <Col md="6" className="d-flex justify-content-end">
                                    <Button color="primary" id="btnMake" onClick={this.handleSubmit}>Gerar Meu Campeonato
                                        </Button>
                                </Col>
                            </Row>
                            <Row className="text-left">

                                {this.state.movies.map(movie =>
                                    <Col md="3" className="mt-2">
                                        <Card key={movie.id}>
                                            <CardBody>
                                                <input type="checkbox" id={movie.id} name="movies" ref="check_me" onClick={this.handleMovie.bind(null, movie)} />
                                                <p className="text-nowrap">{movie.titulo}</p>
                                                <p>{movie.ano}</p>
                                            </CardBody>
                                        </Card>
                                    </Col>
                                )}

                            </Row>
                        </section>
                    </Container>
                </form>
            </div>

        );
    }
}

export default MovieList;