import React from 'react';
import { Mask, Row, View, Container, Col} from 'mdbreact';
class AppPage extends React.Component {
    render() {
        return (
            <div id="apppage">
                <View>
                    <Mask className="d-flex justify-content-center align-items-center gradient">
                        <Container id="home">
                            <Row className="text-center">
                                <Col><h1 className="text-white text-center maintitle"> <strong>
                                    COPA FILMES</strong>
                                </h1></Col>
                            </Row>
                        </Container>
                    </Mask>
                </View>
            </div >
        );
    }
};

export default AppPage;