﻿using AutoMapper;
using CopaFilmes.Domain.Entities;
using CopaFilmes.Service.Services;
using CopaFilmes.UI.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace CopaFilmes.UI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class MovieController : Controller
    {
        private MovieService service = new MovieService();
        private readonly IMapper _mapper;

        public MovieController(IMapper mapper)
        {
            _mapper = mapper;
        }


        [HttpGet("[action]")]
        public IEnumerable<MovieViewModel> GetMovies()
        {
            return _mapper.Map<IEnumerable<MovieViewModel>>(service.GetAll());

        }

        [HttpPost("[action]")]
        public MatchViewModel Championship([FromBody] List<MovieViewModel> model)
        {
            var moviesList = _mapper.Map<List<Movie>>(model);
            return _mapper.Map<MatchViewModel>(service.GetWinner(moviesList));
        }


    }
}